#include "std_includes.h"

#include "hwio.h"
#include "timer.h"
#include "display.h"

/*
 * main.c
 */
int main(void) {
    WDTCTL = WDTPW | WDTHOLD;	// Stop watchdog timer
	
    // Initialiser

    hwio_init();

    timer_init();

    timer_start();

    // Main Loop

    for(;;) // Ever
    {
    	hwio_inputHandler();

    	display_buildScene();
    	if(hwio_button1Pressed)
    	{

    	}
    	else
    	{

    	}

		hwio_greenLedOn = hwio_button2Pressed;
		display_outputDisplayBuffer();
    	hwio_outputHandler();
    }

	return 0;
}
