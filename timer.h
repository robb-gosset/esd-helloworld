/*
 * timer.h
 *
 *  Created on: 29 Oct 2015
 *      Author: rg2-gosset
 */

#ifndef TIMER_H_
#define TIMER_H_

extern volatile uint32_t timer_heartbeatMillis;
extern volatile uint32_t timer_hwioUpkeepMillis;

int timer_init();
void timer_start();

#endif /* TIMER_H_ */
