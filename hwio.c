#include "std_includes.h"
#include "hwio.h"

/*
 * hwio.c
 *
 *  Created on: 29 Oct 2015
 *      Author: rg2-gosset
 */

volatile uint8_t hwio_button1Pressed;
volatile uint8_t hwio_button2Pressed;

volatile uint16_t hwio_button1Bucket;
volatile uint16_t hwio_button2Bucket;

volatile uint8_t hwio_redLedOn;
volatile uint8_t hwio_greenLedOn;

// Hardware IO initialiser
int hwio_init ()
{
	hwio_button1Pressed	= 0;
	hwio_button2Pressed	= 0;

	hwio_button1Bucket = 0;
	hwio_button2Bucket = 0;

	hwio_redLedOn		= 0;
	hwio_greenLedOn		= 0;

    PM5CTL0 &= ~LOCKLPM5;

    P1DIR |= 0x01; // P1.1 to output
    P4DIR |= 0x40; // P4.6 to output

    P1DIR &= ~0x02;
    P1REN |= 0x02;

    P4DIR &= ~0x20;
    P4REN |= 0x20;

    return 0;
}

void hwio_upkeep ()
{
	if(hwio_BUTTON2BIT == 0)
	{
		if (hwio_button2Bucket > 0x2000)
		{
			// Do nothing	
		}
		else
		{
			hwio_button2Bucket += 0x100;
		}	
	}
	if(hwio_button2Bucket != 0)
	{
		hwio_button2Bucket -= 0x80;
	}
}

// I Handler function, must be called periodically
void hwio_inputHandler ()
{
	if (hwio_button2Bucket > 0x1000)
	{
		hwio_button2Pressed = 1;
	}
	else
	{
		hwio_button2Pressed = 0;
	}

	if(hwio_BUTTON1BIT == 0)
	{
		hwio_button1Pressed = 1;
	}
	else
	{
		hwio_button1Pressed = 0;
	}
}

// O handler
void hwio_outputHandler ()
{
	if(hwio_redLedOn)
	{
		hwio_LEDREDON;
	}
	else
	{
		hwio_LEDREDOFF;
	}

	if(hwio_greenLedOn)
	{
		hwio_LEDGREENON;
	}
	else
	{
		hwio_LEDGREENOFF;
	}

}



