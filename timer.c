#include "std_includes.h"
#include "timer.h"

#include "hwio.h"

/*
 * Timer.c
 *
 *  Created on: 29 Oct 2015
 *      Author: rg2-gosset
 */

volatile uint32_t timer_heartbeatMillis = 0;
volatile uint32_t timer_hwioUpkeepMillis = 0;

int timer_init ()
{
    TA0CCR0 = 1024;
    TA0CCTL0 = 0x10;
    TA0CTL = TASSEL_2 + MC_1;

    return 0;
}

void timer_start ()
{
	_BIS_SR(GIE);
}

#pragma vector=TIMER0_A0_VECTOR
__interrupt void Timer0_A0 (void) // Timer0 A0 1ms
{
	// Toggle the red LED 100ms on and 400ms off

	timer_heartbeatMillis++;

	if(timer_heartbeatMillis >= 950 )
	{
		if(timer_heartbeatMillis >= 1000 )
		{
			hwio_redLedOn = 0;
			timer_heartbeatMillis = 0;
		}
		else
		{
			hwio_redLedOn = 1;
		}
	}

	timer_hwioUpkeepMillis ++;

	if(timer_hwioUpkeepMillis >= 50 )
	{
		timer_hwioUpkeepMillis = 0;
		hwio_upkeep();
	}
}


