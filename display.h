/*
 * display.h
 *
 *  Created on: 5 Nov 2015
 *      Author: robb-gosset
 */

#ifndef DISPLAY_H_
#define DISPLAY_H_

int display_init();

void display_buildScene();
void display_outputDisplayBuffer();
void display_initDisplayBuffer(char setting);

extern char display_reverse(char inchar);
extern char display_DisplayBuffer[96][96/8];
extern const char Tank[8][4];
extern const char Font[8][16];

#endif /* DISPLAY_H_ */
