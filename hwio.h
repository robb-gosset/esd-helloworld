/*
 * hwio.h
 *
 *  Created on: 29 Oct 2015
 *      Author: rg2-gosset
 */

#ifndef HWIO_H_
#define HWIO_H_

// Input bits
#define hwio_BUTTON1BIT 	(P4IN & 0x10)
#define hwio_BUTTON2BIT		(P1IN & 0x02)

// Macros to turn green LED on and off
#define hwio_LEDGREENON		P1OUT |= 0x01
#define hwio_LEDGREENOFF	P1OUT &= (0xFF-0x01)

// Macros to turn red LED on and off
#define hwio_LEDREDON 		P4OUT |= 0x40
#define hwio_LEDREDOFF		P4OUT &= (0xFF-0x40)

// Externs
extern volatile uint8_t hwio_button1Pressed;
extern volatile uint8_t hwio_button2Pressed;

extern volatile uint16_t hwio_button1Bucket;
extern volatile uint16_t hwio_button2Bucket;

extern volatile uint8_t hwio_redLedOn;
extern volatile uint8_t hwio_greenLedOn;

// HWIO Initialiser
int hwio_init ();

// HWIO kmilisecond upkeep
void hwio_upkeep ();

// I Handler function, must be called periodically
void hwio_inputHandler ();

// O handler
void hwio_outputHandler ();

#endif /* HWIO_H_ */
